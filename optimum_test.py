from optimum_benchmark.optimum_benchmark.backends.pytorch.config import PyTorchConfig
from optimum_benchmark.optimum_benchmark.benchmarks.inference.config import InferenceConfig
from optimum_benchmark.optimum_benchmark.experiment import ExperimentConfig, launch
from optimum_benchmark.optimum_benchmark.launchers.process.config import ProcessConfig
from optimum_benchmark.optimum_benchmark.logging_utils import setup_logging


import torch


if __name__ == "__main__":
    setup_logging(level="INFO")
    launcher_config = ProcessConfig(device_isolation=False)
    benchmark_config = InferenceConfig(
        memory=True,
        latency=True,
        input_shapes={"batch_size": 8, "sequence_length": 512},
        generate_kwargs={"max_new_tokens": 512, "min_new_tokens": 512},
    )
    backend_config = PyTorchConfig(
        model="franchukpetro/Mistral-7b-v0.1-awq-int4",
        device="cuda",
        device_ids="0",
        # torche_dtype = "float16",
        no_weights=False,
       quantization_scheme="awq",
       # quantization_config={"version": "exllama"},
    )
    experiment_config = ExperimentConfig(
        experiment_name="mistral-fp16-base",
        benchmark=benchmark_config,
        launcher=launcher_config,
        backend=backend_config,
    )
    benchmark_report = launch(experiment_config)
    benchmark_report.to_json("mistral7b_awq_int4_batch_size_8.json", flat=False)