from awq import AutoAWQForCausalLM
from transformers import AutoModelForCausalLM, AutoTokenizer, GPTQConfig
from AutoGPTQ.auto_gptq import AutoGPTQForCausalLM, BaseQuantizeConfig

def gptq_quantize(model_id, path_quantized):
    quantize_config = BaseQuantizeConfig(
    bits=4,  
    group_size=128, 
    desc_act=False,
    )


    tokenizer = AutoTokenizer.from_pretrained(model_id, use_fast=True)

    examples = [ tokenizer(
        "auto-gptq is an easy-to-use model quantization library with user-friendly apis, based on GPTQ algorithm."
    )
]

    quantized_model = AutoGPTQForCausalLM.from_pretrained(model_id, 
                                                quantize_config)
    quantized_model.quantize(examples)
    
    quantized_model.save_quantized(path_quantized)


def awq_quantize(model_id, path_quantized):
    quant_config = { "zero_point": True, "q_group_size": 128, "w_bit": 4 }

    model = AutoAWQForCausalLM.from_pretrained(model_path)
    tokenizer = AutoTokenizer.from_pretrained(model_path)
    tokenizer.save_pretrained(path_quantized)

    model.quantize(tokenizer, quant_config=quant_config)

    model.save_quantized(path_quantized)
    tokenizer.save_pretrained(path_quantized)

model_path = 'mistralai/Mistral-7B-v0.1'
quant_path_gptq = 'quantized/Mistral-7B-v0.1-GPTQ'
quant_path_awq = 'quantized/Mistral-7B-v0.1-AWQ'


gptq_quantize(model_path, quant_path_gptq)
awq_quantize(model_path, quant_path_awq)